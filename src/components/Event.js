import React from "react";
import Moment from "react-moment";

const Event = (props) => {
  return (
    <>
      <div section-scroll="3" className="wd_scroll_wrap">
        <div className="wd_event_wrapper wd_toppadder90 wd_bottompadder50">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div
                  className="wd_heading wow fadeInDown"
                  data-wow-delay="0.3s"
                >
                  <h4>Upacara & pesta</h4>
                  <h1>ACARA PERNIKAHAN</h1>
                  <img
                    src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
                    alt="Heading"
                    className="img-responsive"
                  />
                </div>
              </div>
              {props.data &&
                props.data.events.map((data, i) => {
                  var className;
                  if (i % 2 === 0) {
                    className = "col-lg-4 col-md-4 col-sm-12 col-xs-12";
                  } else {
                    className =
                      "col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-push-4 col-md-push-4 col-sm-push-0";
                  }
                  return (
                    <>
                      <div className={className}>
                        <div className="wd_event_infobox">
                          <h2>{data.name}</h2>
                          <h1>
                            <Moment format="HH:mm">{data.startDate}</Moment>-
                            <Moment format="HH:mm">{data.endDate}</Moment>
                          </h1>
                          <span>{data.address}</span>
                          <p>{data.description}</p>
                          <div className="clearfix"></div>
                          <div className="wd_btn wd_single_index_menu_rsvp">
                            <a href onClick={() => props.onAction(data)}>
                              Add to calendar
                            </a>
                          </div>
                          <br />
                          <br />
                          <br />
                          <a
                            href={`http://maps.google.com/maps?z=12&t=m&q=loc:${data.lat}+${data.lng}`}
                            target="blank"
                          >
                            <img
                              src="http://webstrot.com/html/wedding/home_2/images/content/map.png"
                              alt="Map"
                              className="img-responsive"
                            />
                          </a>
                        </div>
                      </div>
                    </>
                  );
                })}
              {/* <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="wd_event_infobox">
                  <h2>Main Ceremony</h2>
                  <h1>7:30 pm</h1>
                  <span>St. Thomas's Church, London, U.K.</span>
                  <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor vel velit auctor aliquet. Aenean sollicitudin, lorem quis <a href="#">Read More...</a></p>
                  <div className="clearfix"></div>
                  <a href="#"><img src="http://webstrot.com/html/wedding/home_2/images/content/map.png" alt="Map" className="img-responsive"/></a>
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-push-4 col-md-push-4 col-sm-push-0">
                <div className="wd_event_infobox">
                  <h2>Wedding Party</h2>
                  <h1>7:30 pm</h1>
                  <span>St. Thomas's Church, London, U.K.</span>
                  <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor vel velit auctor aliquet. Aenean sollicitudin, lorem quis <a href="#">Read More...</a></p>
                  <div className="clearfix"></div>
                  <a href="#"><img src="http://webstrot.com/html/wedding/home_2/images/content/map.png" alt="Map" className="img-responsive"/></a>
                </div>
              </div> */}
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-pull-4 col-md-pull-4 col-sm-pull-0">
                <div className="wd_event_infobox">
                  <img
                    src="http://webstrot.com/html/wedding/home_2/images/content/event_img.gif"
                    alt="Event"
                  />
                  {/* <div className="wd_btn wd_single_index_menu_rsvp">
                    <a href="#rsvp">rsvp</a>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Event;
