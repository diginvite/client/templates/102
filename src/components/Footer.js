import React from "react";

const Footer = (props) => {
  return (
    <>
      <div
        className="wd_footer_bottom_wrapper wd_toppadder50 wd_bottompadder50"
        style={{ backgroundColor: "white" }}
      >
        <a
          href
          id="scroll"
          title="Scroll to Top"
          // style="display: none;"
        >
          <i className="fa fa-long-arrow-up" aria-hidden="true"></i>
        </a>
        <div className="col-md-12">
          <div className="wd_footer_section">
            <p style={{ color: "black", float: "left" }}>
              <a
                href="https://instagram.com/diginvite_"
                target="blank"
                style={{ color: "#6b6ee9" }}
              >
                <i className="fa fa-instagram fa-lg"></i>
              </a>
              &nbsp;&nbsp;
              <a
                href="https://twitter.com/diginvite_"
                target="blank"
                style={{ color: "#6b6ee9" }}
              >
                <i className="fa fa-twitter fa-lg"></i>
              </a>
              &nbsp;&nbsp;
              <a
                href="https://facebook.com/diginvitte"
                target="blank"
                style={{ color: "#6b6ee9" }}
              >
                <i className="fa fa-facebook-official fa-lg"></i>
              </a>
            </p>
            <p style={{ color: "black" }}>
              @ Copyright 2017 All Rights Reserved. By{" "}
              <a
                href="https://diginvite.com"
                target="blank"
                style={{ color: "#6b6ee9" }}
              >
                Diginvite
              </a>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
