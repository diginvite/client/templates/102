import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ToastContainer, toast } from "react-toastify";

// var wishes = [];
const settings = {
	dots: true,
	infinite: true,
	// speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	speed: 3000,
	autoplaySpeed: 2000,
};

const Guest = (props) => {
	const [formInput, setFormInput] = useState({
		name: "",
		form: "",
		person: 1,
		attend: 1,
		wish: "",
	});

	const onChange = (e) => {
		const { name, value } = e.target;
		const newInput = { ...formInput, [name]: value };
		setFormInput(newInput);
	};

	const onSubmit = (e) => {
		e.preventDefault();
		if (formInput.name === "" || formInput.wish === "") {
			return toast.error("Name and wish are required", {
				position: toast.POSITION.TOP_RIGHT,
			});
		}
		props.onSubmit(formInput);
		setFormInput({
			name: "",
			form: "",
			person: 1,
			attend: 1,
			wish: "",
		});
	};

	return (
		<>
			<div section-scroll="4" className="wd_scroll_wrap" id="wd_contect_wrapper">
				<div className="wd_guest_wrapper wd_toppadder90">
					<div className="wd_overlay"></div>
					<div className="container">
						<div className="row">
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div className="wd_heading wow fadeInDown" data-wow-delay="0.3s">
									<h4>Silahakn diisi</h4>
									<h1>JADILAH TAMU KAMI!</h1>
									<img
										src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
										alt="Heading"
										className="img-responsive"
									/>
								</div>
							</div>
							{/* <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="wd_guest_infobox">
                  <h2>You’re Invited <span>Jenny & Mark</span> Wedding</h2>
                  <h4>Please Rsvp Before 15 May 2017</h4>
                  <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor This is Photoshop's version  of Lorem Ipsum.</p>
                  <h2><span>Gift Registry</span></h2>
                  <p>What we want most for our wedding is to have our friends and family there to celebrate the occasion with us. If you would like to get us something, w’d love that too. you can find our registries here :</p>
                </div>
                <div className="wd_guest_slider">
                  <div className="owl-carousel owl-theme">
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue1.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue2.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue3.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue1.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue2.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue3.png" alt="Slider" className="img-responsive"/></div>
                    <div className="item"><img src="http://webstrot.com/html/wedding/home_2/images/content/gue1.png" alt="Slider" className="img-responsive"/></div>
                  </div>
                </div>
              </div> */}
							<div className="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
							<div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div className="wd_guest_formbox">
									<span>Apakah Anda Hadir??</span>
									<h4>Isi Disini!</h4>
									<div className="">
										<div className="p-2">
											<form onSubmit={onSubmit}>
												<div className="">
													<label>Nama :</label>
													<input
														type="text"
														name="name"
														placeholder="Nama*"
														className="form-control"
														value={formInput.name}
														onChange={(e) => onChange(e)}
													/>
												</div>
												<div className="">
													<label>Siapanya siapa? :</label>
													<input
														type="text"
														name="form"
														placeholder="Co. rekan kerja"
														className="form-control"
														value={formInput.form}
														onChange={(e) => onChange(e)}
													/>
												</div>
												<div className="">
													<label>Hadir :</label>
													{/* <select name="guest_no" className="require">
                          <option value="0">00</option>
                          <option value="1">01</option>
                          <option value="2">02</option>
                        </select> */}
													<select
														className="form-control"
														name="attend"
														value={formInput.attend}
														onChange={(e) => onChange(e)}
													>
														<option value="1">Hadir</option>
														<option value="2">Tentatif</option>
														<option value="0">Tidak Hadir</option>
													</select>
												</div>
												<div className="">
													<label>Jumlah Tamu :</label>
													{/* <select name="event_name" className="require">
                          <option value="all">All Events</option>
                          <option value="party">Party Events</option>
                          <option value="marriage">Marriage Events</option>
                        </select> */}
													<select
														className="form-control"
														name="person"
														value={formInput.person}
														onChange={(e) => props.onChange(e)}
													>
														<option value="1">1 orang</option>
														<option value="2">2 orang</option>
														{/* <option>3</option>
                            <option>4</option> */}
													</select>
												</div>
												<div className="">
													<label>Ucapan dan doa :</label>
													<textarea
														className="form-control"
														rows="5"
														name="wish"
														placeholder="Ucapan dan Do'a*"
														value={formInput.wish}
														onChange={(e) => onChange(e)}
													></textarea>
												</div>
												<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<div className="response"></div>
												</div>
												<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-2">
													<div className="wd_btn">
														<button type="submit" className="submitForm" form-type="inquiry">
															Kirim
														</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wd_testimonial_wrapper wd_toppadder70 wd_bottompadder70">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="wd_testimonial_slider">
									<div class="owl-theme">
										<Slider {...settings}>
											{props.data.detail.wishes.map((wish, index) => {
												return (
													<div class="item" key={index}>
														<div class="wd_testimo_box">
															<img
																src="http://webstrot.com/html/wedding/home_2/images/content/heart.png"
																alt="Testimonial"
															/>
															<p>“ {wish.wish} ”</p>
															<h4>~ {wish.name} ~</h4>
														</div>
													</div>
												);
											})}
										</Slider>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<ToastContainer />
		</>
	);
};

export default Guest;
