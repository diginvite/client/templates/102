import React from "react";
import Moment from "react-moment";

const Story = (props) => {
  return (
    <>
      <div section-scroll="2" className="wd_scroll_wrap">
        <div className="wd_story_wrapper wd_toppadder90 wd_bottompadder90">
          <div className="wd_overlay"></div>
          <div className="container">
            <div className="row">
              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div
                  className="wd_heading wow fadeInDown"
                  data-wow-delay="0.3s"
                >
                  {/* <h4>Jenny & Mark</h4> */}
                  <h1>Kisah cinta kami</h1>
                  <img
                    src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
                    alt="Heading"
                    className="img-responsive"
                  />
                </div>
              </div>
              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="wd_story_covers">
                  <div className="wd_story_line"></div>
                  {props.data &&
                    props.data.stories.map((data, i) => {
                      var className;
                      if (i % 2 === 0) {
                        className = "wd_story_covers_box_left";
                      } else {
                        className = "wd_story_covers_box_right";
                      }
                      return (
                        <>
                          <div className={className}>
                            <div className="wd_story_dot">
                              <span></span>
                            </div>
                            {/* <div className="wd_story_covers_box_img">
                              <img src="http://webstrot.com/html/wedding/home_2/images/content/story_1.jpg" alt="Story"/>
                            </div> */}
                            <div
                              className="wd_story_covers_box_datails"
                              style={{ width: "100%", marginBottom: "30px" }}
                            >
                              <span>
                                {/* 20 May 2010 */}
                                <Moment format="DD MMM. YYYY">
                                  {data.date}
                                </Moment>
                              </span>
                              <h1>{data.title}</h1>
                              {/* <span>That day changed Life</span> */}
                              <p>{data.description}</p>
                            </div>
                          </div>
                        </>
                      );
                    })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Story;
