import React, { useEffect, useState } from "react";
import Moment from "react-moment";

let coupleInit = "";
let x = "";

const Header = ({ data }) => {
	const [cover, setCover] = useState({ url: "" });
	const [couple, setCouple] = useState("");

	useEffect(() => {
		const findCovers = data.images.map((image) => {
			if (image.isCover === true) {
				return image;
			}
		});
		if (findCovers.length > 0) {
			setCover(findCovers[0]);
		}

		for (let i = 0; i < data.couples.length; i++) {
			if (i === 0) {
				x = " & ";
			} else {
				x = " ";
			}
			coupleInit += data.couples[i].nickName + x;
		}
		setCouple(coupleInit);
	}, [data]);

	return (
		<>
			<div section-scroll="0" className="wd_scroll_wrap">
				<div
					className="wd_slider_wrapper wd_single_index_menu"
					style={{ backgroundImage: `url(${cover.url})`, height: "90vh" }}
				>
					<div id="snow"></div>
					{/* <div className="wd_header_wrapper">
            <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div className="wd_logo">
                <img src="http://webstrot.com/html/wedding/home_2/images/header/logo.png" alt="Logo" title="Logo" className="img-responsive"/>
                <button className="wd_menu_btn"><i className="fa fa-bars" aria-hidden="true"></i></button>
              </div>
            </div>
            <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
              <div className="wd_mainmenu_wrapper">
                <div className="wd_main_menu_wrapper">
                  <div className="wd_main_menu wd_single_index_menu">
                    <ul>
                      <li><a href="http://webstrot.com/html/wedding/home_2/0">Home</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/1">About Us</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/2">Love Story</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/3">Events</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/4">Rsvp</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/5">Family</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/6">Gallery</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/7">Blog</a></li>
                      <li><a href="http://webstrot.com/html/wedding/home_2/8">Contact Us</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
					<div className="wd_slider_textinfo">
						{/* <img src="http://webstrot.com/html/wedding/home_2/images/header/name.png" alt="Slider Image" className="img-responsive"/> */}
						<p
							style={{
								color: "#ffffff",
								fontFamily: "'Great Vibes', cursive",
								fontSize: "50px",
							}}
						>
							{couple}
						</p>
						<h3>
							{/* 20 may 2018 */}
							<Moment format="DD MMM. YYYY" style={{ color: "#ffffff" }}>
								{data && data.events[0].startDate}
							</Moment>
						</h3>
						<h1 style={{ color: "#ffffff" }}>Save The Date</h1>
						{/* <p onClick={() => alert()}>Add to calendar</p> */}
						<div className="clearfix"></div>
						<img
							src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
							alt="Heading"
							className="img-responsive"
						/>
					</div>
					<div className="wd_single_index_menu_down">
						<ul>
							<li>
								<a href="#home" id="headbottom">
									<i className="fa fa-long-arrow-down" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</>
	);
};

export default Header;
