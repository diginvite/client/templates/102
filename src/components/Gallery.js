import React, { useState, useEffect } from "react";

const Gallery = ({ data }) => {
	const [galleries, setGalleries] = useState([]);

	useEffect(() => {
		const newGalleries = data.images.map((image) => {
			if (image.isGallery) {
				return image;
			}
		});
		setGalleries(newGalleries);
	}, [data]);

	return (
		<>
			<div section-scroll="6" className="wd_scroll_wrap">
				<div className="wd_gallery_wrapper wd_toppadder90 wd_bottompadder90">
					<div className="container">
						<div className="row">
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div className="wd_heading wow fadeInDown" data-wow-delay="0.3s">
									{/* <h4>Jenny & Mark</h4> */}
									<h1>Galeri Foto</h1>
									<img
										src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
										alt="Heading"
										className="img-responsive"
									/>
								</div>
							</div>
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div className="wd_gallery_slider popup-gallery">
									<div className="owl-theme">
										<div className="item card-columns">
											{galleries.map((image, i) => {
												return (
													<div className="" key={i}>
														<div className="wd_gallery_slide">
															<img
																src={image.url}
																alt="Wedding"
																style={{ heigh: "100%", width: "100%" }}
															/>
															<div className="ast_glr_overlay">
																<p>{image.description}</p>
																<a href={image.url} title="">
																	<i className="fa fa-expand" aria-hidden="true"></i>
																</a>
															</div>
														</div>
													</div>
												);
											})}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Gallery;
