import React from "react";
import Moment from "react-moment";
import Countdown from "react-countdown-now";

const renderer = ({ days, hours, minutes, seconds }) => {
  return (
    <>
      {/* <div className="box"><div>{days}</div><span>Days</span></div>
    <div className="box"><div>{hours}</div><span>Hours</span></div>
    <div className="box"><div>{minutes}</div><span>Minutes</span></div>
    <div className="box"><div>{seconds}</div><span>Seconds</span></div> */}
      <div>
        {days}
        <div className="smalltext">Days</div>
      </div>
      <div>
        {hours}
        <div className="smalltext">Hours</div>
      </div>
      <div>
        {minutes}
        <div className="smalltext">Minutes</div>
      </div>
      <div>
        {seconds}
        <div className="smalltext">Seconds</div>
      </div>
    </>
  );
};

const About = (props) => {
  return (
    <>
      <div section-scroll="1" className="wd_scroll_wrap" id="down">
        <div className="wd_about_wrapper wd_toppadder90 wd_bottompadder70">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div
                  className="wd_heading wow fadeInDown"
                  data-wow-delay="0.3s"
                >
                  <h4>Akan Menikah!</h4>
                  <h1>Pasangan Bahagia</h1>
                  <img
                    src="http://webstrot.com/html/wedding/home_2/images/content/heading.png"
                    alt="Heading"
                    className="img-responsive"
                  />
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="wd_about_infobox wd_toppadder20">
                  <div className="wd_about_infobox_img">
                    <span>
                      <img
                        src="http://webstrot.com/html/wedding/home_2/images/content/line.png"
                        alt="Line"
                        className="img-responsive"
                      />
                    </span>
                    <img
                      src={props.data && props.data.couples[0].images[0].url}
                      alt="Groom"
                      className="img-responsive"
                      style={{ maxWidth: "270px" }}
                    />
                  </div>
                  <h2>{props.data && props.data.couples[0].fullName}</h2>
                  <p>{props.data && props.data.couples[0].description}</p>
                  {/* <ul>
                    <li><a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul> */}
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-21 col-xs-12 col-lg-push-4 col-md-push-4 col-sm-push-0">
                <div className="wd_about_infobox wd_toppadder20">
                  <div className="wd_about_infobox_img">
                    <span>
                      <img
                        src="http://webstrot.com/html/wedding/home_2/images/content/line.png"
                        alt="Line"
                        className="img-responsive"
                      />
                    </span>
                    <img
                      src={props.data && props.data.couples[1].images[0].url}
                      alt="Groom"
                      className="img-responsive"
                      style={{ maxWidth: "270px" }}
                    />
                  </div>
                  <h2>{props.data && props.data.couples[1].fullName}</h2>
                  <p>{props.data && props.data.couples[1].description}</p>
                  {/* <ul>
                    <li><a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul> */}
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-pull-4 col-md-pull-4 col-sm-pull-0">
                <div className="wd_about_infobox">
                  <h1>Undangan</h1>
                  <div className="wd_about_infobox_date">
                    {/* <p style={{ fontSize: "20px" }}>
                      <b>Halo {props.invite && props.invite.name}</b>
                    </p> */}
                    <p>Kami mengundang Anda untuk merayakan pernikahan kami</p>
                    <h3>
                      <Moment format="dddd" locale="de">
                        {props.data && props.data.events[0].startDate}
                      </Moment>
                    </h3>
                    <h2>
                      {/* 20 May 2018 */}
                      <Moment format="DD MMM. YYYY">
                        {props.data && props.data.events[0].startDate}
                      </Moment>
                    </h2>
                    <p>At {props.data && props.data.events[0].address}</p>
                  </div>
                  {/* <div className="wd_btn wd_single_index_menu_rsvp">
                    <a href="http://webstrot.com/html/wedding/home_2/4">rsvp</a>
                  </div> */}
                </div>
              </div>
              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="wd_timer_wrapper">
                  <div className="wd_center_words">
                    <h4>Jangan lewatkan!</h4>
                  </div>
                  <div id="clockdiv">
                    {/* <div><span className="days"></span><div className="smalltext">Days</div></div>
                    <div><span className="hours"></span><div className="smalltext">Hours</div></div>
                    <div><span className="minutes"></span><div className="smalltext">Minutes</div></div>
                    <div><span className="seconds"></span><div className="smalltext">Seconds</div></div> */}
                    <Countdown
                      date={props.data && props.data.events[0].startDate}
                      renderer={renderer}
                    />
                  </div>
                  <div className="wd_center_words">
                    <h4>sampai kita menikah!</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default About;
