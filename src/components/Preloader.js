import React from 'react';

const Preload = (props) => {
  return(
    <div id="preloader">
      <div id="status"><img src="http://webstrot.com/html/wedding/home_2/images/header/preloader.gif" id="preloader_image" alt="loader"/>
      </div>
    </div>
  )
}

export default Preload;