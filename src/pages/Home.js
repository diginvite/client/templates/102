import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";

import { apiUrl } from "../actions/config";

import "../App.css";

import About from "../components/About";
import Event from "../components/Event";
import Footer from "../components/Footer";
import Gallery from "../components/Gallery";
import Guest from "../components/Guest";
import Header from "../components/Header";
import Preload from "../components/Preloader";
import Story from "../components/Story";
import Modal from "../components/Modal";

class Home extends Component {
	constructor() {
		super();
		this.state = {
			order: null,
			invite: null,
			modal: true,
			music: false,
		};
	}

	componentDidMount = () => {
		var url_string = document.URL;
		var url = new URL(url_string);
		var hostname = url.hostname;
		var hostnames = hostname.split(".");
		var param = url.searchParams.get("invite");

		axios
			.get(`${apiUrl}/findOrder?domain=${hostnames[0]}&invitation=${param}`)
			.then((response) => {
				this.setState({
					order: response.data.order,
					invite: response.data.invite,
				});
			})
			.catch((error) => {
				throw error;
			});
	};

	onAction = (event) => {
		const startDate = moment(event.startDate).format("YYYYMMDD");
		const startTime = moment(event.startDate).format("HHmmss");
		const endDate = moment(event.endDate).format("YYYYMMDD");
		const endTime = moment(event.endDate).format("HHmmss");
		const location = event.address;
		const couple = `${this.state.order.detail.couples[0].fullName} dan ${this.state.order.detail.couples[1].fullName}`;

		var url = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T${startTime}/${endDate}T${endTime}&ctz=Asia/Jakarta&details=Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/Saudari berkenan hadir untuk memberikan doa restu kepada kami.<br><br>Terima kasih,<br><br>${couple}&location=${location}&pli=1&amp;uid=1521339627addtocalendar&sf=true&amp;output=xml`;
		var win = window.open(url, "_blank");
		win.focus();
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onSubmit = (formInput) => {
		const data = {
			name: formInput.name,
			from: formInput.from,
			attend: formInput.attend,
			person: formInput.person,
			wish: formInput.wish,
		};
		axios
			.post(`${apiUrl}/insertOneWish/${this.state.order.id}`, data)
			.then(({data}) => {
				this.setState({order: data})
				toast.success("Data saved", {
					position: toast.POSITION.TOP_RIGHT,
				});
			})
			.catch((error) => {
				throw error;
			});
	};

	onTogglePlay(param) {
		var x = document.getElementById("myAudio");
		if (param === "play") {
			x.play();
		} else {
			x.pause();
		}
		this.setState({ music: !this.state.music });
	}

	render() {
		if (!this.state.order) {
			return <Preload />;
		}

		return (
			<>
				<div className="application">
					<Helmet>
						<meta charSet="utf-8" />
						<title>{this.state.order && this.state.order.name}</title>
					</Helmet>
				</div>
				<Header
					data={this.state.order.detail}
					onAction={(flag) => this.onAction(flag)}
				/>
				<About data={this.state.order.detail} invite={this.state.invite} />
				<Story data={this.state.order.detail} />
				<Event
					data={this.state.order.detail}
					onAction={(flag) => this.onAction(flag)}
				/>
				<Guest
					data={{ ...this.state.order, guests: [] }}
					onChange={(e) => this.onChange(e)}
					onSubmit={(e) => this.onSubmit(e)}
				/>
				<Gallery data={this.state.order.detail} />
				<Footer />
				<ToastContainer />
				<audio
					loop=""
					preload="auto"
					id="myAudio"
					src={this.state.order.detail.songs[0].url}
				/>
				{this.state.invite ? (
					<Modal
						visible={this.state.modal}
						invite={this.state.invite}
						onClose={() => {
							this.setState({ modal: false });
						}}
					/>
				) : null}
				{!this.state.music ? (
					<a
						href="#home"
						class="music"
						onClick={() => this.onTogglePlay("play")}
						style={{ backgroundColor: "#6b6ee9" }}
					>
						<i style={{ color: "#ffffff" }} class="fa fa-music"></i>
					</a>
				) : (
					<a
						href="#home"
						class="music"
						style={{ backgroundColor: "#6b6ee9" }}
						onClick={() => this.onTogglePlay("pause")}
					>
						<i style={{ color: "#ffffff" }} class="fa fa-volume-up"></i>
					</a>
				)}
			</>
		);
	}
}

export default Home;
